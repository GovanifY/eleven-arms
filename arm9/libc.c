#include "libc.h"

void *memset(void * ptr, int value, size_t num)
{
    byte *p = ptr;
    while (num) {
        *p++ = value;
        num--;
    }
    return ptr;
}

void *memcpy(void *destination, const void *source, size_t num)
{
    byte *dest = destination;
    byte *src  = (byte*)source;
    while (num) {
        *dest++ = *src++;
        num--;
    }
    return destination;
}

static void reverse(char *s, int count)
{
    int i;
    for (i = 0; i < (count/2); i++) {
        char tmp = s[i];
        s[i] = s[count-i-1];
        s[count-i-1] = tmp;
    }
}

int u32tostr(unsigned int n, char *out)
{
    char *p = out;
    int cnt = 0;
    do {
        *p++ = (n%10) + '0';
        n /= 10;
        cnt++;
    } while (n);
    *p = '\0';
    reverse(out, cnt);
    return cnt;
}
