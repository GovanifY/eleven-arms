#include "draw.h"
#include "libc.h"
#include "utils.h"
#include "3dstypes.h"

extern void *stage1_bin;
extern int   stage1_bin_size;
#define      STAGE1_ADDR       ((volatile u32*)0x1FFF4B40)
#define      MAGIC_ADDR        ((volatile u32*)0x1FFFFFF8)
#define      ARM11_EXCVEC_ADDR ((volatile u32*)0x1FFF4000)

unsigned int excvec_backup[8];

//ARM9 running on SVC with IRQs, FIQs, and MPU disabled
int main()
{
    ClearScreen();
    font_draw_string(10, 10, WHITE, "Eleven ARMs by xerpi");

    //Backup arm11 exception vectors
    memcpy(excvec_backup, (void*)ARM11_EXCVEC_ADDR, sizeof(excvec_backup));
    font_draw_string(10, 20, WHITE, "Backed up ARM11 SWI");
    //Copy stage1 payload
    memcpy((void*)STAGE1_ADDR, &stage1_bin, stage1_bin_size);
    font_draw_string(10, 30, WHITE, "stage1 payload copied");
    //Set magic number to 0...
    *MAGIC_ADDR = 0;
    font_draw_string(10, 40, WHITE, "Magic number set to 0");
    //Overwrite ARM11 exception vectors with "b stage1_addr"
    font_draw_string(10, 50, WHITE, "Overwriting ARM11 exception vectors...");
    ARM11_EXCVEC_ADDR[0] = 0xea0002ce;
    ARM11_EXCVEC_ADDR[1] = 0xea0002cd;
    ARM11_EXCVEC_ADDR[2] = 0xea0002cc;
    ARM11_EXCVEC_ADDR[3] = 0xea0002cb;
    ARM11_EXCVEC_ADDR[4] = 0xea0002ca;
    ARM11_EXCVEC_ADDR[5] = 0xea0002c9;
    ARM11_EXCVEC_ADDR[6] = 0xea0002c8;
    ARM11_EXCVEC_ADDR[7] = 0xea0002c7;
    
    font_draw_string(10, 60, WHITE, "Waiting for ARM11 control...");

    while (1) {
        draw_fillrect(10, 70, 8*12, 20, BLACK);
        font_draw_u32(10, 70, GREEN, *MAGIC_ADDR);
    }
    
    //Restore arm11 exception vectors
    memcpy((void*)ARM11_EXCVEC_ADDR, excvec_backup, sizeof(excvec_backup));
    
    return 0;
}
