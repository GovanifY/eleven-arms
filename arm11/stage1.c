#define MAGIC_ADDR  ((volatile unsigned int*)0xEFFFFFF8)

#define FB_TOP_LEFT1  (0xF0184E60)
#define FB_TOP_LEFT2  (0xF01CB370)
#define FB_TOP_RIGHT1 (0xF0282160)
#define FB_TOP_RIGHT2 (0xF02C8670)
#define FB_BOT_1      (0xF02118E0)
#define FB_BOT_2      (0xF0249CF0)
#define FB_TOP_SIZE   (0x46500)
#define FB_BOT_SIZE   (0x3BC40)
void ClearScreen();


//APPCORE code
int main()
{
    while (1) {
        *MAGIC_ADDR = *MAGIC_ADDR + 1;
        ClearScreen();
    }
    return 0;
}

void ClearScreen()
{
    int i;
    for (i = 0; i < FB_TOP_SIZE; i+=4) {
        *(int *)(FB_TOP_LEFT1 + i) = 0;
        *(int *)(FB_TOP_LEFT2 + i) = 0;
        *(int *)(FB_TOP_RIGHT1 + i) = 0;
        *(int *)(FB_TOP_RIGHT2 + i) = 0;
    }
    for (i = 0; i < FB_BOT_SIZE; i+=4) {
        *(int *)(FB_BOT_1 + i) = 0;
        *(int *)(FB_BOT_2 + i) = 0;
    }
}
